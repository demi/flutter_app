import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class GameChannel {
  //static const MethodChannel _channel = const MethodChannel('my_game');
  static const BasicMessageChannel _string_channel =
      const BasicMessageChannel('my_game', StringCodec());
  static const BasicMessageChannel _capture_channel =
      const BasicMessageChannel('cap', BinaryCodec());

  void capturePointer() {
    // Tell platform to capture mouse button events and send them to Flutter.
    //_string_channel.send("Capture pointer!");
    var data = Uint8List.fromList([1]).buffer.asByteData();
    _capture_channel.send(data);
    //_capture_channel.binaryMessenger.send("cap", data);
  }

  void releasePointer() {
    // Tell platform not to capture mouse button events for flutter,
    // and instead handle the events itself.
    //_string_channel.send("Release pointer!");
    var data = Uint8List.fromList([0]).buffer.asByteData();
    _capture_channel.send(data);
  }
}
