import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_app/game_channel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //debugRepaintRainbowEnabled = true;
    return MaterialApp(
      //checkerboardRasterCacheImages: true,
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.dark,
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
        backgroundColor: Colors.transparent,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class MoveableThingState extends State<MoveableThing>
    with SingleTickerProviderStateMixin {
  double x = 0;
  double y = 0;
  double w = 100;
  double h = 100;
  bool dragging = false;
  bool resizing = false;
  bool resizingLeft = false;
  bool resizingRight = false;
  bool resizingTop = false;
  bool resizingBottom = false;

  AnimationController controller;
  Animation<double> rainbow;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: Duration(seconds: 3), vsync: this);
    rainbow = Tween<double>(begin: 0.0, end: 360.0).animate(controller);
    controller.repeat();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var gestureNode = GestureDetector(
      onPanEnd: (_) {
        setState(() {
          resizing = false;
          dragging = false;
          resizingLeft = false;
          resizingRight = false;
          resizingTop = false;
          resizingBottom = false;
        });
      },
      onPanUpdate: (tapInfo) {
        debugPrint('========');
        debugPrint('local: ${tapInfo.localPosition}');
        debugPrint('delta: ${tapInfo.delta}');
        debugPrint('size: $w; $h');
        debugPrint('========');
        var wMin = 100.0;
        var hMin = 100.0;
        setState(() {
          if (!resizing && !dragging) {
            const zone = 50;
            if (tapInfo.localPosition.dx < zone) {
              resizingLeft = true;
              resizing = true;
            } else if (tapInfo.localPosition.dx > w - zone) {
              resizingRight = true;
              resizing = true;
            }

            if (tapInfo.localPosition.dy < zone) {
              resizingTop = true;
              resizing = true;
            } else if (tapInfo.localPosition.dy > h - zone) {
              resizingBottom = true;
              resizing = true;
            }

            if (!resizing) {
              dragging = true;
            }
          }
          if (dragging) {
            x += tapInfo.delta.dx;
            y += tapInfo.delta.dy;
          } else {
            if (resizingLeft) {
              x += tapInfo.delta.dx;
              w = max(wMin, w - tapInfo.delta.dx);
            } else if (resizingRight) {
              var xRight = x + w + tapInfo.delta.dx;
              w = max(wMin, w + tapInfo.delta.dx);
              x = xRight - w;
            }
            if (resizingTop) {
              y += tapInfo.delta.dy;
              h = max(hMin, h - tapInfo.delta.dy);
            } else if (resizingBottom) {
              var yBottom = y + h + tapInfo.delta.dy;
              h = max(hMin, h + tapInfo.delta.dy);
              y = yBottom - h;
            }
          }
        });
      },
      child: FocusableActionDetector(
        onShowHoverHighlight: (hover) {
          if (hover) {
            controller.repeat();
          } else if (controller.isAnimating) {
            controller.stop();
          }
        },
        child: AnimatedBuilder(
          animation: rainbow,
          builder: (BuildContext context, Widget child) {
            return Container(
              width: 256,
              height: 256,
              decoration: BoxDecoration(
                  color:
                      HSLColor.fromAHSL(1.0, rainbow.value, 0.8, 0.6).toColor(),
                  borderRadius: BorderRadius.circular(10)),
              child: Center(
                child: Text(
                  'Test :D',
                ),
              ),
            );
          },
        ),
      ),
    );
    return Positioned(
      top: y,
      left: x,
      width: w,
      height: h,
      child: RepaintBoundary(child: gestureNode),
    );
  }
}

class MoveableThing extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MoveableThingState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  double ptrX = 0.0;
  double ptrY = 0.0;
  double globalPtrX = 0.0;
  double globalPtrY = 0.0;

  double frametime = 0.0;

  GameChannel game = GameChannel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var tPrev = Duration(seconds: 0);
    SchedulerBinding.instance.addPersistentFrameCallback((timestamp) {
      var dt = (timestamp - tPrev).inMicroseconds * 1E-6;
      tPrev = timestamp;
      Future.delayed(Duration.zero, () {
        if (dt > 0 && (dt - frametime).abs() > 1E-4) {
          setState(() {
            frametime = dt;
          });
        }
      });
    });
  }

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    var bgMouseEvents = MouseRegion(
      onHover: (event) {
        //debugPrint('background ptr hover: $event');
        setState(() {
          ptrX = event.localPosition.dx;
          ptrY = event.localPosition.dy;
        });
      },
      onEnter: (event) {
        debugPrint('background ptr onEnter: $event');
        game.releasePointer();
      },
      onExit: (event) {
        debugPrint('background ptr onExit: $event');
        game.capturePointer();
      },
      child: Listener(
        onPointerDown: (event) {
          debugPrint('background ptr down: $event');
        },
        onPointerMove: (event) {
          debugPrint('background ptr move: $event');
          setState(() {
            ptrX = event.localPosition.dx;
            ptrY = event.localPosition.dy;
          });
        },
      ),
    );

    var bodyStack = Stack(
      // Center is a layout widget. It takes a single child and positions it
      // in the middle of the parent.
      fit: StackFit.expand,
      children: <Widget>[
        bgMouseEvents,
        MoveableThing(),
        Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(color: Colors.white),
            ),
            Text(
              'Pointer position:',
            ),
            Text(
              '${ptrX.toStringAsFixed(2)} ${ptrY.toStringAsFixed(2)}',
              style: Theme.of(context).textTheme.headline6,
            ),
            Text(
              'Global pointer position:',
            ),
            Text(
              '${globalPtrX.toStringAsFixed(2)} ${globalPtrY.toStringAsFixed(2)}',
              style: Theme.of(context).textTheme.headline6,
            ),
            Text(
              'Framerate:',
            ),
            Text(
              '${frametime > 0 ? (1.0 / frametime).toStringAsFixed(2) : "N/A"}',
              style: Theme.of(context).textTheme.headline6,
            ),
          ],
        ),
        Positioned(
          left: 10,
          right: 10,
          top: 10,
          height: 200,
          child: PerformanceOverlay.allEnabled(),
        ),
      ],
    );

    var fgMouseEvents = MouseRegion(
      opaque: false,
      onHover: (event) {
        //debugPrint('foreground ptr hover: $event');

        setState(() {
          globalPtrX = event.position.dx;
          globalPtrY = event.position.dy;
        });
      },
      child: Listener(
        onPointerDown: (event) {
          debugPrint('foreground ptr down: $event');
        },
        onPointerMove: (event) {
          debugPrint('foreground ptr move: $event');
          setState(() {
            globalPtrX = event.position.dx;
            globalPtrY = event.position.dy;
          });
        },
      ),
    );

    return Stack(
      fit: StackFit.expand,
      children: [
        Scaffold(
          appBar: AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            title: Text(widget.title),
          ),
          backgroundColor: Colors.transparent,

          body: RawKeyboardListener(
            onKey: (event) {
              debugPrint('onKey: $event');
              if (event.isKeyPressed(LogicalKeyboardKey.escape)) {
                SystemNavigator.pop(animated: true);
              }
            },
            focusNode: FocusNode(),
            autofocus: true,
            child: bodyStack,
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: _incrementCounter,
            tooltip: 'Increment',
            child: Icon(Icons.add),
          ), // This trailing comma makes auto-formatting nicer for build methods.
        ),
        fgMouseEvents,
      ],
    );
  }
}
